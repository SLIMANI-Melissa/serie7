package exo15;


	

	import java.io.BufferedReader;
	import java.io.FileInputStream;
	import java.io.IOException;
	import java.io.InputStreamReader;
    import java.util.Comparator;
    import java.util.Map;
    import java.util.Set;
	import java.util.function.Function;
   import java.util.stream.Collector;
    import java.util.stream.Collectors;
	import java.util.stream.Stream;
	import java.util.zip.GZIPInputStream;
	import java.util.Map.Entry;






	public class ActorsAndMovies {

	    public static void main(String[] args) {

	        ActorsAndMovies actorsAndMovies = new ActorsAndMovies();
	        Set<Movie> movies = actorsAndMovies.readMovies();
	        
	      //Question1:
	            System.out.println("\nle nombre de film\n" + movies.size());
	        
	      //Question2:
	       long nmbracteur = 
	            movies.stream().flatMap(movie->movie.actors().stream()).distinct().count();
	            System.out.println("\nle nombre d�acteur\n"+ nmbracteur);
	       
	     //Question3:
	       long nmbrannee = 
	            movies.stream().map(movie->movie.releaseYear()).distinct().count();
	            System.out.println("\nle nombre d�annee\n"+ nmbrannee);
	       
	     //Question4:
	       int  vieuxfilmAnnee = 
	   			movies.stream().map(movie->movie.releaseYear()).min(Integer::compare).get();
	   			System.out.println( "\nl�ann�e de sortie du film le plus vieux\n "+vieuxfilmAnnee);
	   	   int  recentfilmAnnee = 
	   	        movies.stream().map(movie->movie.releaseYear()).max(Integer::compare).get();
	   	        System.out.println( "\nl�ann�e de sortie du film le plus recent\n "+recentfilmAnnee); 
	   	        
	   	 //Question5:
	   	   Map<Integer, Long> anneenmbrfilm = 
	   			movies.stream().collect(Collectors.groupingBy(movie->movie.releaseYear(),Collectors.counting()));
	   			Entry<Integer, Long> maxfilmAnnee = anneenmbrfilm.entrySet().stream().max(Comparator.comparing(Entry::getValue)).get();
	   		    System.out.println("\nl'ann�e de sortie du plus grand nombre de films est en " + maxfilmAnnee.getKey() + " avec " +maxfilmAnnee.getValue() + " films");
	   	        
	   	//Question6:
	 	  Movie maxActeurFilm =
	 	        movies.stream().max(Comparator.comparing(movie-> movie.actors().size())).get();
	 	        System.out.println("\n le film qui contient le plus grand nombre d�acteurs est intitul�: " + maxActeurFilm.title() + " avec " + maxActeurFilm.actors().size() + " acteurs");
	 	        
	 	//Question7:
	 	  Map<Actor, Long> acteurs = 
	 		   	movies.stream().flatMap(m -> m.actors().stream()).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
	 		   	Entry<Actor, Long> acteurmaxfilm = acteurs.entrySet().stream().max(Comparator.comparing(Entry::getValue)).get();
	 		    System.out.println("\n l'acteur  qui a jou� dans le plus grand nombre de films  est :" + acteurmaxfilm.getKey().lastName +" "+ acteurmaxfilm.getKey().firstName + " avec " +acteurmaxfilm.getValue() + " films");
	   //Question8:
	 		   Collector<Movie, Integer, Entry<Actor, Long>> collector = 
	 	        		Collectors.collectingAndThen(Collectors.flatMapping(movie -> movie.actors().stream(),
	 	        				Collectors.groupingBy(Function.identity(),Collectors.counting())),
	 							map -> map.entrySet().stream().max(Comparator.comparing(Entry::getValue)).get());
	 	        
	 	        Map<Integer, Entry<Actor, Long>> maxActeurAnnee = 
	 	        		movies.stream().collect(Collectors.groupingBy(Movie::releaseYear,collector));
	 	        Entry <Integer, Entry<Actor, Long>> acteurMaxdefilm =
	 	    		  maxActeurAnnee.entrySet().stream().max(Comparator.comparing(e -> e.getValue().getValue())).get();
	 			System.out.println( " \n acteur  aui a jou� dans le plus grand nombre de films en une ann�e est "+acteurMaxdefilm.getValue().getKey().lastName + " "
	 	      		+ acteurMaxdefilm.getValue().getKey().firstName + " il a jou� dans" + acteurMaxdefilm.getValue().getValue()+ " films en " + acteurMaxdefilm.getKey());		    
	 		   		    
	 		   		
	    }

	    public Set<Movie> readMovies() {

	        Function<String, Stream<Movie>> toMovie =
	                line -> {
	                    String[] elements = line.split("/");
	                    String title = elements[0].substring(0, elements[0].lastIndexOf("(")).trim();
	                    String releaseYear = elements[0].substring(elements[0].lastIndexOf("(") + 1, elements[0].lastIndexOf(")"));
	                    if (releaseYear.contains(",")) {
	                        int indexOfComa = releaseYear.indexOf(",");
	                        releaseYear = releaseYear.substring(0,indexOfComa);
	                    }
	                    Movie movie = new Movie(title, Integer.valueOf(releaseYear));


	                    for (int i = 1; i < elements.length; i++) {
	                        String[] name = elements[i].split(", ");
	                        String lastName = name[0].trim();
	                        String firstName = "";
	                        if (name.length > 1) {
	                            firstName = name[1].trim();
	                        }

	                        Actor actor = new Actor(lastName, firstName);
	                        movie.addActor(actor);
	                    }
	                    return Stream.of(movie);
	                };

	        try (FileInputStream fis = new FileInputStream("files/movies-mpaa.txt.gz");
	             GZIPInputStream gzis = new GZIPInputStream(fis);
	             InputStreamReader reader = new InputStreamReader(gzis);
	             BufferedReader bufferedReader = new BufferedReader(reader);
	             Stream<String> lines = bufferedReader.lines();
	        ) {

	            return lines.flatMap(toMovie).collect(Collectors.toSet());

	        } catch (IOException e) {
	            System.out.println("e.getMessage() = " + e.getMessage());
	        }

	        return Set.of();
	    }
	}


